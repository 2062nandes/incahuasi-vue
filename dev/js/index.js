//Slider principal
import { tns } from "./modules/tiny-slider"
//Animacion de click Scrooll
import SmoothScroll from "smooth-scroll/dist/js/smooth-scroll.min"
//WOW JS
import { WOW } from 'wowjs/dist/wow.min'
//Headroom JS
import Headroom from 'headroom.js/dist/headroom.min'

//Data vuejs
import { contactform } from './modules/contactform'
import { menuinicio, mainmenu } from './modules/menus'

import Vue from 'vue/dist/vue.min'
import VueResource from 'vue-resource/dist/vue-resource.min'

Vue.use(VueResource);

new Vue({
  el: '#app',
  data: {
    showmodal: false,
    toggle: false,
    formSubmitted: false,
    vue: contactform,
    menuinicio,
    mainmenu,
    isActive: true,
    error: null
  },
  created: () => {
    let wow = new WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: false,
      live: false
    })
    wow.init()
    let deplazamiento;
    const mediumBp = matchMedia('(min-width: 680px)');
    const changeSize = mql => {
      mql.matches
        ? deplazamiento = 118
        : deplazamiento = 65
    };
    mediumBp.addListener(changeSize);
    changeSize(mediumBp);
    var scroll = new SmoothScroll('a[href*="#"]', {
      // Selectors
      ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
      header: null, // Selector for fixed headers (must be a valid CSS selector)

      // Speed & Easing
      speed: 500, // Integer. How fast to complete the scroll in milliseconds
      offset: deplazamiento, // Integer or Function returning an integer. How far to offset the scrolling anchor location in pixels
      easing: 'easeInOutCubic', // Easing pattern to use
      customEasing: function (time) { }, // Function. Custom easing pattern

      // Callback API
      before: function () { }, // Callback to run before scroll
      after: function () { } // Callback to run after scroll
    });

    //FOOTER AÑO
    function getDate() {
      var today = new Date()
      var year = today.getFullYear()
      document.getElementById('currentDate').innerHTML = year
    }
    getDate()
  },
  mounted: () => {
    var header = new Headroom(document.querySelector('#header'), {
      tolerance: 15,
      offset: 160,
      tolerance: {
        up: 5,
        down: 0
      },
      classes: {
        initial: 'header-initial', //Cuando el elemento se inicializa
        pinned: 'header-up', //Cuando se deplaza hacia arriba
        unpinned: 'header-down', //Cuando se deplaza hacia abajo
        top: 'header-top', //Cuando esta pegado arriba
        notTop: 'header-notop', //Cuando no esta pegado arriba
        bottom: 'header-bottom', //Cuando esta pegado abajo
        notBottom: 'header-nobottom'//Cuando no esta pegado abajo
      }
    })
    header.init()

    let slider = tns({
      container: '.slider-incahuasi',
      mode:'gallery',
      controlsText: [
        '<',
        '>'
      ],
      autoplayText: ['▶', '❚❚'],
      autoplay: true,
      speed: 2000,
      lazyload: true,
      animateIn: 'flipInY',
      animateOut: 'bounceOut'
    })
    let carousel = tns({
      container: '.carousel',
      controls: false,
      responsive: {
        "550": {
          "items": 4
        },
        "840": {
          "items": 8
        },
        "1024": {
          "items": 10
        }
      },

      // autoplayDirection: 'backward',
      autoplayText: ['▶', '❚❚'],
      autoplay: true,
      autoplayTimeout: 1500,
      speed: 1000,
      lazyload: true
    })
  },
  methods: {
    isFormValid: function () {
      return this.nombre != ''
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('/mail.php', { vue: this.vue }).then(function (response) {
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    toggleMenu: function () {
      if (this.toggle == true) {
        this.toggle = false
        console.log('DESACTIVADO')
      }
      else {
        this.toggle = true
        console.log('ACTIVO')
      }
    },
    mainMenu: function () {
      if (menuinicio[1].active)
      { 
        this.menuinicio[1].active = false;
      }else{
        this.menuinicio[1].active = true;
      }
    }
  }
})