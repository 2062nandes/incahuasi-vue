export const mainmenu = [
  {
    title: 'Aire acondicionado',
    href: 'aire-acondicionado',
  },
  {
    title: 'Calor calefacción',
    href: 'calor-calefaccion',
  },
  {
    title: 'Gas Natural',
    href: 'gas-natural',
  },
  {
    title: 'Gases Medicinales',
    href: 'gases-medicinales',
  }
];

export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
  },
  {
    title: 'Nosotros',
    href: 'nosotros',
    active: false
  },
  {
    title: 'Contactos',
    href: 'contactos',
  }
];